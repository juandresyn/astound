# Astound Commerce Test!

## Dependencies

To Run this project, you will need.

Node.js (last version works)

NPM

## How to run

Clone this repo.

`npm install`

`gulp`

### Or

Open project folder, look for `app/index.html` and open it in your browser

## Live Demo

[Astound Commerce Live!](https://juandresyn.bitbucket.io/astound/)

## Overview & Description

Pure Javascript is being used here, no plugins or third party libs are included (in the test files files)

Node.js and Gulp (NPM packages) are ment for development process and not for logic applied into the final files (feel free to confirm this).

### Personal Libs

I am using pre-made (by myself) libs such as: [browser.js](https://github.com/Juandresyn/browser.js) and [vanilla-slider](https://github.com/Juandresyn/vanilla-slider). Both are pure-vanilla Javascript.

### Additional specs added

* A disabled state for product variants (sizes)
* An hover state for Featured Products


## Tested in

### Desktop
* Chrome Mac(last)
* Chrome Windows(last)
* Firefox Mac(last)
* Firefox Windows(last)
* Safari Mac(last)
* Internet Explorer Windows(9)
* Edge Windows(last)

### Mobile
* Safari Mobile (iOs 10)
* Chrome Mobile (Android N)