(function(){

  'use strict';

  var js = false;
  var el = window.document.documentElement;
  var classes = {
        css: {
          opera: 'opera',
          chrome: 'chrome',
          safari: 'safari',
          firefox: 'firefox',
          facebook: 'facebook',
          ie: 'ie',
          edge: 'edge',
          unknown: 'unknown'
        },
        js: {
          opera: 'js-opera',
          chrome: 'js-chrome',
          safari: 'js-safari',
          firefox: 'js-firefox',
          facebook: 'js-facebook',
          ie: 'js-ie',
          edge: 'js-edge',
          unknown: 'js-unknown'
        }
      };

  function detectBrowser() {
    var is;
    var browser = navigator.userAgent;
    var _css = classes.css;
    var _js = classes.js;

    if(browser.indexOf('Chrome') !== -1 ){
      if(browser.indexOf('OPR') !== -1 ) {
        // Opera uses Chrome as it's engine, so, check it under Chrome's userAgent.
        is = _css.opera + ((js) ? ' ' + _js.opera: '');
      }else if(browser.indexOf('Edge') !== -1 ) {
        is = _css.edge + ((js) ? ' ' + _js.edge: '');
      }else{
        is = _css.chrome + ((js) ? ' ' + _js.chrome: '');
      }
    }else if(browser.indexOf('Safari') !== -1){
      is = _css.safari + ((js) ? ' ' + _js.safari: '');
    }else if(browser.indexOf('Firefox') !== -1 ) {
      is = _css.firefox + ((js) ? ' ' + _js.firefox: '');
    }else if(browser.indexOf('FBAN') !== -1 || browser.indexOf('FBAV') > -1) {
      is = _css.facebook + ((js) ? ' ' + _js.facebook: '');
    }else if((browser.indexOf('MSIE') !== -1 ) || (document.documentMode === true )){
      // If IE > 10
      is = _css.ie + ((js) ? ' ' + _js.ie: '');
    }else {
      is = _css.unknown + ((js) ? ' ' + _js.unknown: '');
    }

    return is;
  }

  function browser() {
    var classString = el.className;
    var newClass = classString.concat(' ' + detectBrowser());
    el.className = newClass;
  }

  window.onload = setTimeout(browser, 500);
})();
