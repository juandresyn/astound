(function() {
  'use strict';

  var share = document.querySelector('.js-share-tooltip');
  var shareOpen = document.querySelector('.js-share-open');
  var shareClose = document.querySelector('.js-share-close');
  var hideClass = 'vanish';

  shareOpen.onclick = function(){
    share.classList.remove(hideClass);
  };

  shareClose.onclick = function(){
    share.classList.add(hideClass);
  };
})();
