(function() {
  'use strict';

  var activeClass = 'active';
  var btnNext = document.querySelector('.js-next');
  var btnPrev = document.querySelector('.js-prev');
  var slide = document.querySelectorAll('.js-slider .js-slider-item');
  var i = 0;

  // For Touch Support
  var touchstartX = 0;
  var touchstartY = 0;
  var touchendX = 0;
  var touchendY = 0;

  function slideActions(ac) {
    slide[i].classList.remove(activeClass);

    if (ac === 'next'){
      i--;
      if( i < 0) {
        i = slide.length - 1;
      }
    }else if (ac === 'prev') {
      i++;
      if(i >= slide.length) {
        i = 0;
      }
    }

    slide[i].classList.add(activeClass);

    return false;
  }

  function handleGesure() {
    if (touchendX < touchstartX) {
      slideActions('prev');
    }
    if (touchendX > touchstartX) {
      slideActions('next');
    }

    return false;
  }

  var touchZone = document.querySelector('.js-slider');

  touchZone.addEventListener('touchstart', function(event) {
    touchstartX = event.changedTouches[0].screenY;
    touchstartY = event.changedTouches[0].screenX;
  }, false);

  touchZone.addEventListener('touchend', function(event) {
    touchendX = event.changedTouches[0].screenY;
    touchendY = event.changedTouches[0].screenX;
    handleGesure();
  }, false);

  btnPrev.onclick = function(){
    slideActions('prev');
  };

  btnNext.onclick = function(){
    slideActions('next');
  };
})();
